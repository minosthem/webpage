+++
title = "Professional Experience"
date = "2019-11-17"
sidemenu = "true"
description = "Professional Experience"
+++  

**HEDNO SA - Financial Administrator**<br>
**Islands Network Operation Department**<br>
**November 2017 - July 2018**<br>

* Issues purchase orders to energy producers (RES).
* Gathers and presents monthly and annual data of all the energy producers.
* Communicates daily with customers.
* Collaborates with PPC and RAE on issues about energy pricing and verification of payable amounts.
* Assists the Legal Department providing them the appropriate data to defend the company's interests.
*Competences: familiarized with the energy market, learned to handle large amount of data using excel

**AXIA Ventures Group LTD - Financial Analyst (Intern)**<br>
**Equity Research Department - Market Clearing Section**<br>
**May 2016 - August 2016**<br>

* Researched economic indicators of Greece using EUROSTAT, Hellenic Statistical Authority and the IMF sources.
* Organized and visualized research data using MS Excel
* Analyzed and updated balance sheets of companies of interest, assessed their future economic situation and analyzed political news that affect the economy
* Indexed daily and weekly reports
* Competences: trained in executing financial analysis and forecast reports, worked in a large team where everyone needed input from others to do their work

**Bank of Greece - Intern**<br>
**Department of Legal Entities' Reserves Management Section**<br>
**August 2014 - July 2015**<br>

* Entered general ledger and balance data to indicate liquidity
* Participated in processing of financing agreements through buy/sell back and reverse repos with commercial banks and the public debt management agency
* Recorded interest, settlement and termination of buy/sell back and reverse repos agreements in a database
* Competences: speed and accuracy under workload and deadlines pressure, learned Bloomberg and Reuters software

**Hellenic Army General Staff**<br>
**Security and Information Department**<br>
**November 2016 - July 2017**<br>

* Managed, collected and archived documents
* Organized and distributed daily tasks
* Planned events
