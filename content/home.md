+++
title = "Minos Themelis"
date = "2019-11-17"
sidemenu = "true"
description = "About me"
+++ 

Minos is a junior data scientist with demonstrated experience in Financial Analytics, holding a B.Sc. in Business Economics and Finance 
from the Athens University of Economics and Business. He is currently pursuing M.Sc. studies in Business Analytics and Operations Research 
at Tilburg University, focusing in machine learning, neural networks, statistics, problem solving, and programming. He is passionate in 
helping companies to develop their strategy using data analysis and machine learning techniques.
You can get a pdf of my detailed cv in [English](/pdfs/cv.pdf).

## Personal Info

* Birth date and place: 13/02/1993 | Marousi, Athens
* Marital Status: Single
* Email: minosthemelis@gmail.com

## Computer Skills

* Python
* R
* SPSS Statistics software
* EViews
* Bloomberg and Reuters software
* MS Office (word, excel, power point, access)

## Foreign Languages

* English (IELTS)
* German(Abitur)

## Interests

* Kung Fu/Kick Boxing Semi Contact, Greek championship: 1st place (2003), 2nd place (2003), 2nd place (Cup) 2004, 3rd place (2005)
* Football: National school tournament, in the presence of the former  coach of the national football team, Otto Rehhagel (2nd place), 
A.S.I.S. 2007 (2nd place), A.S.I.S.  2008 (3rd place), 2011-today formation of a team and participation in various tournaments (captain)
* Volleyball: 2010-2012 participation in amateur volleyball club at Melissia
* Participation in a music band 2010-2012 (bassist, performed in the 2011 Rock Concert at the German School of Athens)
