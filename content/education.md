+++
title = "Education"
date = "2014-04-09"
sidemenu = "true"
description = "Education"
+++

**M.Sc. in Business Analytics and Operations Research**<br>
**Tilburg University**<br>
**February 2019 - today**<br>

#### Main Projects:
* Data Science projects for classification and Artificial Neural Networks, implemented in Python (used scikit-learn, PyTorch, numpy)
* Simulation and optimization using stochastic models implemented in Python (using scipy, numpy, gurobipy etc)
* Data Science projects implemented in R using multiple classification and regression algorithms (e.g. LASSO, Logistic Regression, LDA etc)
* Case study for supply chain optimization, based on Albert Hein's data, implemented
* MILP and heuristic implementations for supply chain optimization
* Business plan and market analysis for real-time translation products

**B.Sc. in Economic Science, GPA: 7.0**<br>
**Athens University of Economics and Business**<br>
**October 2011- March 2017**<br>

Specialization: Business Economics and Finance

### List of seminars

* E-learning program "Introduction to programming with python" - Aristotle University of Thessaloniki 16/10/2017-27/11/2017(grade: 97%)
* Workshop of the AEGEE-Athens and AEGEE-Pireus  EAD on Volunteerism and Youth Entrepreneurship
* Participation in the 11th meeting of  Model United Nations at the German School of Athens